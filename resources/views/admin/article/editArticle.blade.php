@extends('layouts.admin')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <form class="mx-auto" style="margin:50px !important" action="{{ route('article.update', ['article' => $article->id]) }}"
        method="post" enctype="multipart/form-data">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Title:</label>
            <input type="text" class="form-control" name="title" value="{{ $article->title }}">
        </div>

        <!-- Textarea without sizing class -->
        <div data-mdb-input-init class="form-outline mb-4">
            <label class="form-label" for="textAreaExample6">Short Desciption:</label>
            <textarea class="form-control" id="textAreaExample6" name="shortDescription" rows="3">{{ $article->shortDescription }}</textarea>
        </div>

        <div class="form-group">
            <label for="sel1">Select an Category:</label>
            <select class="form-control" id="sel1" name="category_id">
                <option>Choose an option:</option>
                @foreach ($categories as $key => $category)
                    <option {{ in_array($article->category_id, $category->pluck('id')->toArray()) ? 'selected' : '' }}
                        value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="editor">Write content:</label>
            <textarea id="editor" name="content">{!! $article->title !!}</textarea>
        </div>

        <div class="form-group">
            <label for="exampleFormControlSelect2">Select tags:</label>
            <select multiple class="form-control" id="exampleFormControlSelect2" name="tags[]">
                @foreach ($tags as $tag)
                    <option {{ in_array($tag->id, $article->tags->pluck('id')->toArray()) ? 'selected' : '' }}
                        value="{{ $tag->id }}">{{ $tag->name }}</option>
                @endforeach

            </select>
        </div>

        <div class="form-group" style="margin:30px 0px !important;">
            <div class="mb-3">
                <label for="formFile" class="form-label">Select an image:</label>
                <input class="form-control" type="file" id="formFile" name="files">
            </div>
        </div>

        <div class="form-group" style="margin:30px 0px !important;">
            <div class="mb-3 center">
                <img style="width: 350px;" src="{{ Storage::url($article->image) }}" alt="">

            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <script src="https://cdn.ckeditor.com/ckeditor5/41.1.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'), {
                ckfinder: {
                    uploadUrl: `{{ route('content.image.upload') . '?_token=' . csrf_token() }}`
                }
            })
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>

@endsection
@section('js')
    @include('layouts.js')
@endsection
@section('css')
    @include('layouts.css')
@endsection
