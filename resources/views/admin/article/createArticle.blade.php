@extends('layouts.admin')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <form class="mx-auto" style="margin:50px !important" action="{{ route('article.store') }}" method="post"
        enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Title:</label>
            <input type="text" class="form-control" name="title">
        </div>

        <!-- Textarea without sizing class -->
        <div data-mdb-input-init class="form-outline mb-4">
            <label class="form-label" for="textAreaExample6">Short Desciption:</label>
            <textarea class="form-control" id="textAreaExample6" name="shortDescription" rows="3"></textarea>
        </div>

        <div class="form-group">
            <label for="sel1">Select an Category:</label>
            <select class="form-control" id="sel1" name="category_id">
                <option>Choose an option:</option>
                @foreach ($categories as $key => $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>


        <div class="form-group">
            <label for="editor">Write content:</label>
            <textarea id="editor" name="content"></textarea>
        </div>

        <div class="form-group">
            <label for="exampleFormControlSelect2">Select tags:</label>
            <select multiple class="form-control" id="exampleFormControlSelect2" name="tags[]">
                @foreach ($tags as $tag)
                    <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group" style="margin:30px 0px !important;">
            <div class="mb-3">
                <label for="formFile" class="form-label">Select an image:</label>
                <input class="form-control" type="file" id="formFile" name="files" onchange="handleFileChange(event)">
            </div>
            <div id="selectedFiles1"></div>
            <div id="showText"></div>
        </div>



        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <script src="https://cdn.ckeditor.com/ckeditor5/41.1.0/classic/ckeditor.js"></script>
    <script>
        function handleFileSelectStory(e) {
            var selDiv = document.querySelector("#selectedFiles1");
            var showText = document.querySelector("#showText");
            if (!e.target.files || !window.FileReader) return;
            selDiv.innerHTML = "";
            var files = e.target.files;
            var filesArr = Array.prototype.slice.call(files);
            var yourTextImage = '<p class="mb-2" >Images that you selected:</p>';
            showText.innerHTML = yourTextImage;
            filesArr.forEach(function(f, i) {
                var f = files[i];
                if (!f.type.match("image.*")) {
                    return;
                }
                var reader = new FileReader();
                reader.onload = function(e) {
                    var html = '<img class="size" src="' + e.target.result + '">' + '<br clear="left"/>';
                    selDiv.innerHTML += html;

                }
                reader.readAsDataURL(f);
            });
        }

        ClassicEditor
            .create(document.querySelector('#editor'), {
                ckfinder: {
                    uploadUrl: `{{ route('content.image.upload') . '?_token=' . csrf_token() }}`
                }
            })
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
@section('js')
    @include('layouts.js')
@endsection
@section('css')
    @include('layouts.css')
@endsection
