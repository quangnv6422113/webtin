@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">All articles</h1>

        <div class="d-flex justify-content-between">
            <a href="{{ route('article.create.differ') }}" class="btn btn-success m-10 mb-4">Create an article</a>

            <form class="d-none d-sm-inline-block form-inline my-2 my-md-0 mw-100 navbar-search"
                action="{{ route('article.search') }}" method="get">
                <div class="input-group">
                    <input type="text" class="form-control bg-light border-6 small border border-primary mr-2"
                        name="title" placeholder="Search for title..." aria-label="Search"
                        aria-describedby="basic-addon2">
                    <input type="text" class="form-control bg-light border-6 small border border-primary " name="author"
                        placeholder="Search for author..." aria-label="Search" aria-describedby="basic-addon2">

                    <div class="input-group-append ml-2">
                        <div class="form-group">
                            <select class="form-control" name="categoryName">
                                <option value="">Search by category</option>
                                @foreach ($categories as $key => $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group ml-3">
                            <select class="form-control" name="tag">
                                <option value="">Search by Tag</option>
                                @foreach ($tags as $key => $tag)
                                    <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Table show all articles</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>{{ __('Stt') }}</th>
                                <th>{{ __('Title') }}</th>
                                <th>{{ __('Publish at') }}</th>
                                <th>{{ __('Author') }}</th>
                                <th style="with:110px" !important>{{ __('Image') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($articles as $key => $article)
                                <tr>
                                    <td>{{ $key++ }}</td>
                                    <td>{{ $article->title }}</td>
                                    <td>{{ $article->publish_at }}</td>
                                    <td>{{ $article->author }}</td>
                                    <td><img style="width: 210px !important;" src="{{ Storage::url($article->image) }}"
                                            alt=""></td>
                                    <td>
                                        <a class="btn btn-warning"
                                            href="{{ route('article.edit', ['article' => $article->id]) }}">Edit</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modalDeleteArticle{{ $article->id }}">Delete</button>

                                    </td>
                                </tr>
                                <!-- Modal delete category-->
                                <div class="modal fade" id="modalDeleteArticle{{ $article->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="modalDeleteArticle{{ $article->id }}"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header bg-info text-dark">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure want to
                                                    delete?</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{ route('article.destroy', ['article' => $article->id]) }}"
                                                method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <div class="modal-footer ">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Delete</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- Button trigger modal -->
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    @include('layouts.js')
@endsection
@section('css')
    @include('layouts.css')
@endsection
