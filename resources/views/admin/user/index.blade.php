@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Tables</h1>

        <div class="d-flex justify-content-between">
            <button type="button" class="btn btn-success m-10 mb-4" data-toggle="modal" data-target="#modalCreateUser">
                Create a category
            </button>

            <form class="d-none d-sm-inline-block form-inline my-2 my-md-0 mw-100 navbar-search"
                action="{{ route('user.search') }}" method="get">
                <div class="input-group">
                    <input type="text" class="form-control bg-light border-6 small border border-primary mr-2" name="name"
                        placeholder="Search for name..." aria-label="Search" aria-describedby="basic-addon2">
                    <input type="email" class="form-control bg-light border-6 small border border-primary " name="email"
                        placeholder="Search for email..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <!-- Modal crete category-->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="modal fade" id="modalCreateUser" tabindex="-1" role="dialog" aria-labelledby="modalCreateCategory"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info text-dark">
                        <h5 class="modal-title" id="exampleModalLongTitle">Form create User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('users.store') }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" name="password" value="{{ old('password') }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="sel1">Select role:</label>
                                <select class="form-control" id="sel1" name="role">
                                    <option class="User">User</option>
                                    <option class="Writer">Writer</option>
                                    <option class="Admin">Admin</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer ">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>{{ __('Stt') }}</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Role') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $key => $user)
                                <tr>
                                    <td>{{ $key++ }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role }}</td>
                                    <td>
                                        <button type="button" class="btn btn-warning" data-toggle="modal"
                                            data-target="#modalEditUser{{ $user->id }}">Edit</button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modalDeleteUser{{ $user->id }}">Delete</button>
                                    </td>
                                </tr>
                                <!-- Modal delete category-->
                                <div class="modal fade" id="modalDeleteUser{{ $user->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="modalDeleteUser{{ $user->id }}"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header text-dark bg-info">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure want to
                                                    delete?</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{ route('users.destroy', ['user' => $user->id]) }}"
                                                method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <div class="modal-footer ">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Delete</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <!-- model update -->
                                <div class="modal fade" id="modalEditUser{{ $user->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="modalEditUser{{ $user->id }}"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header bg-info text-dark">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Form update User</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{ route('users.update', ['user' => $user->id]) }}"
                                                method="post">
                                                @method('put')
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Name</label>
                                                        <input type="text" name="name" value="{{ $user->name }}"
                                                            class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Email</label>
                                                        <input readonly type="email" name="email"
                                                            value="{{ $user->email }}" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="sel1">Select role:</label>
                                                        <select class="form-control" id="sel1" name="role">
                                                            <option {{ $user->role == 'User' ? 'selected' : '' }}
                                                                class="User">User</option>
                                                            <option {{ $user->role == 'Writer' ? 'selected' : '' }}
                                                                class="Writer">Writer</option>
                                                            <option {{ $user->role == 'Admin' ? 'selected' : '' }}
                                                                class="Admin">Admin</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer ">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    @include('layouts.js')
@endsection
@section('css')
    @include('layouts.css')
@endsection
