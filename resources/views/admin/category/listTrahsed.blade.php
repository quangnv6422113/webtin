@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
       
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
            For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official
                DataTables documentation</a>.</p>

        <button type="button" class="btn btn-success m-10 mb-4" data-toggle="modal" data-target="#modalCreateCategory">
            Create a category
        </button>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>{{ __('Stt') }}</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Description') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $key => $categorie)
                                <tr>
                                    <td>{{ $key++ }}</td>
                                    <td>{{ $categorie->name }}</td>
                                    <td>{{ $categorie->discription }}</td>
                                    <td>
                                        <button type="button" class="btn btn-warning" data-toggle="modal"
                                            data-target="#modalEditCategory{{ $categorie->id }}">Edit</button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modalDeleteCategory{{ $categorie->id }}">Delete</button>
                                    </td>
                                </tr>
                                <!-- Modal delete category-->
                                <div class="modal fade" id="modalDeleteCategory{{ $categorie->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="modalDeleteCategory{{ $categorie->id }}"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header bg-info text-dark">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure want to delete?</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form
                                                action="{{ route('categories.destroy', ['category' => $categorie->id]) }}"
                                                method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <div class="modal-footer ">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Delete</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal update category-->
                                <div class="modal fade" id="modalEditCategory{{ $categorie->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="modalEditCategory{{ $categorie->id }}"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header text-dark">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Form edit category</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{ route('categories.update', ['category' => $categorie->id]) }}"
                                                method="POST">
                                                @method('PUT')
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Name</label>
                                                        <input type="text" name="name" class="form-control"
                                                            value="{{ $categorie->name }}" placeholder="Enter name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Description</label>
                                                        <input type="text" name="description" class="form-control"
                                                            value="{{ $categorie->discription }}"
                                                            placeholder="Enter Description">
                                                    </div>
                                                </div>
                                                <div class="modal-footer ">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Create</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- Button trigger modal -->


                    <!-- Modal crete category-->
                    <div class="modal fade" id="modalCreateCategory" tabindex="-1" role="dialog"
                        aria-labelledby="modalCreateCategory" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-info text-dark">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Form create category</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('categories.store') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" name="name" value="{{ old('name') }}" class="form-control"
                                                placeholder="Enter name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Description</label>
                                            <input type="text" name="description" value="{{ old('description') }}"  class="form-control"
                                                placeholder="Enter Description">
                                        </div>
                                    </div>
                                    <div class="modal-footer ">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    @include('layouts.js')
@endsection
@section('css')
    @include('layouts.css')
@endsection
