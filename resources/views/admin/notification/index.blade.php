@extends('layouts.admin')

@section('content')
    <style>
        .hidden {
            display: none;
        }
    </style>
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Notificaton</h1>

        <div class="d-flex justify-content-between">
            <button type="button" class="btn btn-success m-10 mb-4" data-toggle="modal" data-target="#modalCreateCategory">
                Create Notification
            </button>

            <form class="d-none d-sm-inline-block form-inline my-2 my-md-0 mw-100 navbar-search"
                action="{{ route('notification.search') }}" method="get">
                <div class="input-group">
                    <div class="form-group mr-2">
                        <select class="form-control" id="sel1" name="status">
                            <option>Search by status</option>
                            <option value="dislable">Dislable</option>
                            <option value="public">Public</option>
                        </select>
                    </div>
                    <input type="text" class="form-control bg-light border-6 small border border-primary" name="title"
                        placeholder="Search for name..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Table show all notifications</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>{{ __('Stt') }}</th>
                                <th>{{ __('Title') }}</th>
                                <th>{{ __('Message') }}</th>
                                <th>{{ __('Sent_at') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($notifications as $key => $notificaton)
                                <tr>
                                    <td>{{ $key++ }}</td>
                                    <td>{{ $notificaton->title }}</td>
                                    <td>{{ $notificaton->message }}</td>
                                    <td>{{ $notificaton->send_to }}</td>
                                    <td>{{ $notificaton->status }}</td>
                                    <td>
                                        <button type="button" class="btn btn-warning" data-toggle="modal"
                                            data-target="#modalEditNotification{{ $notificaton->id }}">Edit</button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modalDeleteNotification{{ $notificaton->id }}">Delete</button>
                                    </td>
                                </tr>
                                <!-- Modal delete category-->
                                <div class="modal fade" id="modalDeleteNotification{{ $notificaton->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="modalDeleteNotification{{ $notificaton->id }}"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header bg-info text-dark">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure want to
                                                    delete?</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form
                                                action="{{ route('notification.destroy', ['notification' => $notificaton->id]) }}"
                                                method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <div class="modal-footer ">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Delete</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal update category-->
                                <div class="modal fade" id="modalEditNotification{{ $notificaton->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="modalEditNotification{{ $notificaton->id }}"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header bg-info text-dark">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Form update an
                                                    notification</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form
                                                action="{{ route('notification.update', ['notification' => $notificaton->id]) }}"
                                                method="post">
                                                @csrf
                                                @method('put')
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Title</label>
                                                        <input type="text" name="title"
                                                            value="{{ $notificaton->title }}" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Message</label>
                                                        <input type="text" name="message"
                                                            value="{{ $notificaton->message }}" class="form-control">
                                                    </div>

                                                    <div>
                                                        <label for="datetime">Time to send:</label>
                                                        <input type="datetime-local" id="send_at" name="send_at"
                                                            class="form-control" value="{{ $notificaton->send_at }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="sendToUser">Send to:</label>
                                                        <select id="sendToUser" name="send_to" class="form-control">
                                                            @foreach ($sendTos as $sendTo)
                                                                <option value="{{ $sendTo }}">{{ $sendTo }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer ">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- Button trigger modal -->


                    <!-- Modal crete category-->
                    <div class="modal fade" id="modalCreateCategory" tabindex="-1" role="dialog"
                        aria-labelledby="modalCreateCategory" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-info text-dark">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Form create an notification</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('notification.store') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Title</label>
                                            <input type="text" name="title" value="{{ old('Title') }}"
                                                class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Message</label>
                                            <input type="text" name="message" value="{{ old('Message') }}"
                                                class="form-control">
                                        </div>
                                        <div>
                                            <label for="datetime">Time to send:</label>
                                            <input type="datetime-local" id="send_at" name="send_at"
                                                class="form-control" value="{{ date('Y-m-d H:i') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="sendToUser">Send to:</label>
                                            <select id="exampleFormControlSelect1" name="send_to" class="form-control"
                                                onchange="toggleSelect()">
                                                @foreach ($sendTos as $sendTo)
                                                    <option value="{{ $sendTo }}">{{ $sendTo }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group hidden" id="formGroup2">
                                            <label for="exampleFormControlSelect2">Example multiple select</label>
                                            <select multiple class="form-control" name="recipient_ids[]">
                                                <option disabled>Choose user to send:</option>
                                                @foreach ($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="modal-footer ">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function toggleSelect() {
            const selectElement = document.getElementById('exampleFormControlSelect1');
            const formGroup2 = document.getElementById('formGroup2');
            if (selectElement.value === 'Others') {
                formGroup2.classList.remove('hidden');
            } else {
                formGroup2.classList.add('hidden');
            }
        }
    </script>

@endsection
@section('js')
    @include('layouts.js')
@endsection
@section('css')
    @include('layouts.css')
@endsection
