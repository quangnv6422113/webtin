@extends('dashboard')
@section('content')
<h1 class="ml-6 font-bold text-center text-2xl">List of articles</h1>
    <div class="mt-12 flex justify-between items-center mx-36 gap-6">
        <div class="w-[70%] p-4">
            @foreach ($articles as $article)
                <div
                    class="relative block mb-4 bg-white hover:bg-gray-100 transition duration-200 shadow-lg rounded-lg overflow-hidden group">
                    <div class="flex gap-4">
                        <div class="w-[30%]">
                            <img class="w-full h-auto object-cover" src="{{ Storage::url($article->image) }}"
                                alt="Article Image">
                        </div>
                        <div class="w-[70%] p-4">
                            <h1 class="font-bold text-xl mb-2">{{ $article->title }}</h1>
                            <p class="text-gray-700">{{ $article->shortDescription }}</p>
                        </div>
                    </div>
                    <div
                        class="absolute bottom-4 right-4 flex gap-2 opacity-0 group-hover:opacity-100 transition-opacity duration-200">
                        <a href="{{ route('show.article', ['id' => $article->id]) }}"
                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                            Detail
                        </a>
                        @auth
                            @if ($article->isLikedBy())
                                <div class="flex justify-between items-center">
                                    <form action="{{ route('unlike', ['articleId' => $article->id]) }}"
                                        id="toggle1{{ $article->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                            class="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">
                                            Unlike
                                        </button>
                                    </form>
                                    <p class="text-white p-1 bg-gray-500 rounded-md">{{ $article->likes->count() }} Like</p>
                                </div>
                            @else
                                <div class="flex justify-between items-center">
                                    <form action="{{ route('like', ['articleId' => $article->id]) }}"
                                        id="toggle2{{ $article->id }}" method="POST">
                                        @csrf
                                        <button type="submit"
                                            class="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">
                                            Like
                                        </button>
                                    </form>
                                    <p class="text-white p-1 bg-gray-500 rounded-md">{{ $article->likes->count() }} Like</p>
                                </div>
                            @endif
                        @endauth
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
