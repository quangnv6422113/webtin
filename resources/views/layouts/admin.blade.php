<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Blank</title>
    @yield('css')
</head>

<body id="page-top">

    @include('layouts.conntent')
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    @yield('content')
    <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->
    @include('layouts.footer');

    @yield('js')
</body>

</html>
