  <!-- Page Wrapper -->
  <div id="wrapper">

      <!-- Sidebar -->
      <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        
          <!-- Sidebar - Brand -->
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
              <div class="sidebar-brand-icon rotate-n-15">
                  <i class="fas fa-laugh-wink"></i>
              </div>

              <div class="sidebar-brand-text mx-3"> {{__('SB Admin')}}<sup>2</sup></div>
          </a>

          <!-- Divider -->
          <hr class="sidebar-divider my-0">
          @if (Auth::check() && Auth::user()->role == 'Admin')
              <!-- Nav Item - Dashboard -->
              <li class="nav-item">
                  <a class="nav-link" href="index.html">
                      <i class="fas fa-fw fa-tachometer-alt"></i>
                      <span>{{__('Dashboard')}}</span></a>
              </li>

              <li class="nav-item">
                  <a class="nav-link" href="{{ route('categories.index') }}">
                      <i class="fas fa-fw fa-tachometer-alt"></i>
                      <span>{{__('Category')}}</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('users.index') }}">
                      <i class="fas fa-fw fa-tachometer-alt"></i>
                      <span>{{__('Manage writer and user')}}</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('notification.index') }}">
                      <i class="fas fa-fw fa-tachometer-alt"></i>
                      <span>{{__('Notification')}}</span>
                  </a>
              </li>
              <!-- Nav Item - Pages Collapse Menu -->
          @endif

          <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                  aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa-fw fa-cog"></i>
                  <span>{{__('Articles')}}</span>
              </a>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                      <h6 class="collapse-header">Custom Components:</h6>
                      <a href="{{ route('article.index') }}" class="collapse-item">List</a>
                      <a href="{{ route('article.list.schedule') }}" class="collapse-item">List task schedule</a>
                  </div>
              </div>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="{{ route('tag.index') }}">
                  <i class="fas fa-fw fa-tachometer-alt"></i>
                  <span>tags</span>
              </a>
          </li>

          <!-- Divider -->
          <hr class="sidebar-divider">

          <!-- Heading -->
          <div class="sidebar-heading">
              Interface
          </div>

          <!-- Nav Item - Pages Collapse Menu -->
          <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                  aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa-fw fa-cog"></i>
                  <span>Components</span>
              </a>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                      <h6 class="collapse-header">Custom Components:</h6>
                      <a class="collapse-item" href="buttons.html">Buttons</a>
                      <a class="collapse-item" href="cards.html">Cards</a>
                  </div>
              </div>
          </li>

          <!-- Nav Item - Utilities Collapse Menu -->
          <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                  aria-expanded="true" aria-controls="collapseUtilities">
                  <i class="fas fa-fw fa-wrench"></i>
                  <span>Utilities</span>
              </a>
              <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                  data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                      <h6 class="collapse-header">Custom Utilities:</h6>
                      <a class="collapse-item" href="utilities-color.html">Colors</a>
                      <a class="collapse-item" href="utilities-border.html">Borders</a>
                      <a class="collapse-item" href="utilities-animation.html">Animations</a>
                      <a class="collapse-item" href="utilities-other.html">Other</a>
                  </div>
              </div>
          </li>

          <!-- Divider -->
          <hr class="sidebar-divider">

          <!-- Heading -->
          <div class="sidebar-heading">
              Addons
          </div>

          <!-- Nav Item - Pages Collapse Menu -->
          <li class="nav-item active">
              <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages"
                  aria-expanded="true" aria-controls="collapsePages">
                  <i class="fas fa-fw fa-folder"></i>
                  <span>Pages</span>
              </a>
              <div id="collapsePages" class="collapse show" aria-labelledby="headingPages"
                  data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                      <h6 class="collapse-header">Login Screens:</h6>
                      <a class="collapse-item" href="login.html">Login</a>
                      <a class="collapse-item" href="register.html">Register</a>
                      <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                      <div class="collapse-divider"></div>
                      <h6 class="collapse-header">Other Pages:</h6>
                      <a class="collapse-item" href="404.html">404 Page</a>
                      <a class="collapse-item active" href="blank.html">Blank Page</a>
                  </div>
              </div>
          </li>

          <!-- Nav Item - Charts -->
          <li class="nav-item">
              <a class="nav-link" href="charts.html">
                  <i class="fas fa-fw fa-chart-area"></i>
                  <span>Charts</span></a>
          </li>

          <!-- Nav Item - Tables -->
          <li class="nav-item">
              <a class="nav-link" href="tables.html">
                  <i class="fas fa-fw fa-table"></i>
                  <span>Tables</span></a>
          </li>

          <!-- Divider -->
          <hr class="sidebar-divider d-none d-md-block">

          <!-- Sidebar Toggler (Sidebar) -->
          <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
          </div>

      </ul>
      <!-- End of Sidebar -->

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">

          <!-- Main Content -->
          <div id="content">

              <!-- Topbar -->
              <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                <div class="dropdown show">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        language
                    </a>
      
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{route('translate.language', ['language' => 'vi'])}}">VN</a>
                        <a class="dropdown-item" href="{{route('translate.language', ['language' => 'es'])}}">US</a>
                    </div>
                </div>
                  <!-- Sidebar Toggle (Topbar) -->
                  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                      <i class="fa fa-bars"></i>
                  </button>

                  <!-- Topbar Search -->


                  <!-- Topbar Navbar -->
                  <ul class="navbar-nav ml-auto">

                      <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                      <li class="nav-item dropdown no-arrow d-sm-none">
                          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-search fa-fw"></i>
                          </a>
                          <!-- Dropdown - Messages -->
                          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                              aria-labelledby="searchDropdown">
                              <form class="form-inline mr-auto w-100 navbar-search">
                                  <div class="input-group">
                                      <input type="text" class="form-control bg-light border-0 small"
                                          placeholder="Search for..." aria-label="Search"
                                          aria-describedby="basic-addon2">
                                      <div class="input-group-append">
                                          <button class="btn btn-primary" type="button">
                                              <i class="fas fa-search fa-sm"></i>
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </li>

                      <!-- Nav Item - Alerts -->
                      <li class="nav-item dropdown no-arrow mx-1">
                          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-bell fa-fw"></i>
                              <!-- Counter - Alerts -->
                              <span class="badge badge-danger badge-counter">3+</span>
                          </a>
                          <!-- Dropdown - Alerts -->
                          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                              aria-labelledby="alertsDropdown">
                              <h6 class="dropdown-header">
                                  Alerts Center
                              </h6>
                              <a class="dropdown-item d-flex align-items-center" href="#">
                                  <div class="mr-3">
                                      <div class="icon-circle bg-primary">
                                          <i class="fas fa-file-alt text-white"></i>
                                      </div>
                                  </div>
                                  <div>
                                      <div class="small text-gray-500">December 12, 2019</div>
                                      <span class="font-weight-bold">A new monthly report is ready to
                                          download!</span>
                                  </div>
                              </a>
                              <a class="dropdown-item d-flex align-items-center" href="#">
                                  <div class="mr-3">
                                      <div class="icon-circle bg-success">
                                          <i class="fas fa-donate text-white"></i>
                                      </div>
                                  </div>
                                  <div>
                                      <div class="small text-gray-500">December 7, 2019</div>
                                      $290.29 has been deposited into your account!
                                  </div>
                              </a>
                              <a class="dropdown-item d-flex align-items-center" href="#">
                                  <div class="mr-3">
                                      <div class="icon-circle bg-warning">
                                          <i class="fas fa-exclamation-triangle text-white"></i>
                                      </div>
                                  </div>
                                  <div>
                                      <div class="small text-gray-500">December 2, 2019</div>
                                      Spending Alert: We've noticed unusually high spending for your account.
                                  </div>
                              </a>
                              <a class="dropdown-item text-center small text-gray-500" href="#">Show All
                                  Alerts</a>
                          </div>
                      </li>

                      <!-- Nav Item - Messages -->
                      <li class="nav-item dropdown no-arrow mx-1">
                          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-envelope fa-fw"></i>
                              <!-- Counter - Messages -->
                              <span class="badge badge-danger badge-counter">7</span>
                          </a>
                          <!-- Dropdown - Messages -->
                          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                              aria-labelledby="messagesDropdown">
                              <h6 class="dropdown-header">
                                  Message Center
                              </h6>
                              <a class="dropdown-item d-flex align-items-center" href="#">
                                  <div class="dropdown-list-image mr-3">
                                      <img class="rounded-circle" src="img/undraw_profile_1.svg" alt="...">
                                      <div class="status-indicator bg-success"></div>
                                  </div>
                                  <div class="font-weight-bold">
                                      <div class="text-truncate">Hi there! I am wondering if you can help me with a
                                          problem I've been having.</div>
                                      <div class="small text-gray-500">Emily Fowler · 58m</div>
                                  </div>
                              </a>
                              <a class="dropdown-item d-flex align-items-center" href="#">
                                  <div class="dropdown-list-image mr-3">
                                      <img class="rounded-circle" src="img/undraw_profile_2.svg" alt="...">
                                      <div class="status-indicator"></div>
                                  </div>
                                  <div>
                                      <div class="text-truncate">I have the photos that you ordered last month, how
                                          would you like them sent to you?</div>
                                      <div class="small text-gray-500">Jae Chun · 1d</div>
                                  </div>
                              </a>
                              <a class="dropdown-item d-flex align-items-center" href="#">
                                  <div class="dropdown-list-image mr-3">
                                      <img class="rounded-circle" src="img/undraw_profile_3.svg" alt="...">
                                      <div class="status-indicator bg-warning"></div>
                                  </div>
                                  <div>
                                      <div class="text-truncate">Last month's report looks great, I am very happy
                                          with
                                          the progress so far, keep up the good work!</div>
                                      <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                                  </div>
                              </a>
                              <a class="dropdown-item d-flex align-items-center" href="#">
                                  <div class="dropdown-list-image mr-3">
                                      <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60"
                                          alt="...">
                                      <div class="status-indicator bg-success"></div>
                                  </div>
                                  <div>
                                      <div class="text-truncate">Am I a good boy? The reason I ask is because someone
                                          told me that people say this to all dogs, even if they aren't good...</div>
                                      <div class="small text-gray-500">Chicken the Dog · 2w</div>
                                  </div>
                              </a>
                              <a class="dropdown-item text-center small text-gray-500" href="#">Read More
                                  Messages</a>
                          </div>
                      </li>

                      <div class="topbar-divider d-none d-sm-block"></div>

                      <!-- Nav Item - User Information -->
                      <li class="nav-item dropdown no-arrow">
                          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span
                                  class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->name }}</span>
                              @if (auth()->user()->image == null)
                                  <img class="img-profile rounded-circle"
                                      src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAL0AyAMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQUGBwgEAwL/xAA6EAABAwMBAgkKBQUAAAAAAAAAAQIDBAURBgchEhYxQVFVYZTREyIyYnFygZGhwRQVF5LhQkNSsdL/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A3iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIVcGP6g1rp3Tyqy63SCKVP7LV4Un7U3p8QMhBrf9bNH+U4HlK3H+f4fd/syfT+tdO6hVGWq6QSyr/ZcvBk/au9fgBkIIRckgAAAAAAAAAAAAAAAAAAAAAAAACMklJrK7/kOl7lc09OngcrPeXcn1VANZ7X9ps1vmlsGnpUbUNTg1VS3fwPUb29poeWaSV7nyvc971y5zlVVX2rzk1E8tTPJPO9XyyOVz3LyqqrlVPkBOVP3FNJE9r4nuY9i5a5qqip7F5j5gDfmyDabNcJorBqGVHVDk4NLUu3cP1HdvabmycRU88tNPHPA9WSxuRzHJyoqLlFOw9G3f8+0vbbovpVELVf7ybnfVFAuwAAAAAAAAAAAAAAAAAAAAAAADA9tzXu2c3HgZ3OjV2Ojhp/BnhVantTL7YK+1yKiJUwujRehcbl+YHGIPVcKGe3Vs9HVxuZPA9zJGLuVFQ8oAAADqfYk17dnNu4fIrpFb7OGv8nMVvoZ7jWwUdJG58872sjYm9VVTsPTFqZYrBQWuNcpTQtjV3SuN6/MC2AAAAAAAAAAAAAAAAAAAAAAAAIwSANW7V9mSamVbrZkay6NaiSR8iVCJyb+ZTnm422ttdW+luNLLTTsXDo5W4U7XweG6Wa2XaHyNzoaeqjx6M0aOx7OgDizHyPVbbbW3SrZS26llqZ3rhscTcqdUfplovh8P8hps+12PlkyG12a2WmHyNsoaeljx6MMaNz7ekDXmyjZkmmVS63lGvujmqkcfKlOi8u/nU2jgYJAAAAAAAAAAAAAAAAAAAAAAAAAHzklZGxz5HI1jUyrnLhE+JS6u1ZbNJ211bc5UTO6KJq+dKvQhzbrjaJetWTvZNKtNQIvmUkS4THrL/UoG8NR7XtL2Vz4Yah1wqG8rKVMtRfe5DArht8uL3qlus1NE3mWaRz1+mDTWVGQNofrnqnOfIW7HR5J3/Ra27b7cWPRLjZqaVnOsMjmL9cmmScgdRac2vaXvTmQzVDrfUO5GVSYaq+9yGeRyskY18bkcxyZRzVyi/E4hypmWh9ol60nOxkMq1NAq+fSSrlMeqv9KgdYAodI6stmrLa2ttkqLjdLE5fOiXoUvgAAAAAAAAAAAAAAAAAAAFXqO+UmnbNU3S4PRsMDc453rzNTtUtDnXb5qh1wvrbFTSL+God8yIu50q+CbgME1hqeu1VeJbhXvXCqqRRZ82JnMiFFkgAAAAAAAnJAAvtH6nrtK3iK4UD1wioksWfNlZzop1jpy+UmobNTXS3vR0M7c452LztXtQ4wNs7A9UOt99dYqmRfw1dviyu5sqeKbgOigAAAAAAAAAAAAAAAAAB5bnWMt9uqq2VU8nTwuld7Gpn7HGFyrZbhcKmtncrpaiV0j16VVcnVm1aodTbPb29qqiup+Bn3lRPuclfECATjtGO0CATjtGO0CATjtGO0CATjtGO0CD1W2tlt9wpq2BypLTytkYqdKLk82O0AdsWysbcLdS1sWFjqIWytx0OTP3PUYjsqqHVWz2yPcuVSn4CqvqqrfsZcAAAAAAAAAAAAAAAAB8aimgqonRVMTJonekyRqOavwU8XF2ydT2/uzPAswBWcXbJ1Pb+7M8BxdsnU9v7szwLMAVnF2ydT2/uzPAcXbJ1Pb+7M8CzAFZxdsnU9v7szwHF2ydT2/uzPAswBWcXbJ1Pb+7M8BxdsnU9v7szwLMAVnF2ydT2/uzPAcXbJ1Pb+7M8CzAHxpqaClhbDTRMhib6LI2o1E+CH2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//Z">
                              @else
                                  <img class="img-profile rounded-circle"
                                      src="{{ Storage::url(auth()->user()->image) }}">
                              @endif
                          </a>
                          <!-- Dropdown - User Information -->
                          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                              aria-labelledby="userDropdown">
                              <a class="dropdown-item" href="#">
                                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                  Profile
                              </a>
                              <a class="dropdown-item" href="#">
                                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                  Settings
                              </a>
                              <a class="dropdown-item" href="#">
                                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                  Activity Log
                              </a>
                              <div class="dropdown-divider"></div>
                              <form method="POST" action="{{ route('logout') }}">
                                  @csrf
                                  <button type="submit" class="dropdown-item">
                                      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                      {{ __('Log Out') }}
                                  </button>
                              </form>
                          </div>
                      </li>

                  </ul>

              </nav>
