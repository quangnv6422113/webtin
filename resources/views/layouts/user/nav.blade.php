<nav class="bg-white border-gray-200 dark:bg-gray-900">
    <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <a href="https://flowbite.com/" class="flex items-center space-x-3 rtl:space-x-reverse">
            <img src="https://flowbite.com/docs/images/logo.svg" class="h-8" alt="Flowbite Logo" />
            <span class="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">Flowbite</span>
        </a>

        <div class="flex items-center md:order-2 space-x-1 md:space-x-0 rtl:space-x-reverse">
            @auth
                <select
                    class="py-3 mr-2 px-4 pe-9 block w-full bg-gray-100 border-transparent rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-neutral-700 dark:border-transparent dark:text-neutral-400 dark:focus:ring-neutral-600">
                    <option selected="">Hello: {{ Auth::user()->name }}</option>
                </select>
                <a class="m-4 bg-blue-500 rounded-lg p-2 text-white" href="{{ route('writerArticle.index') }}">View my all
                    articles </a>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button class="p-3 bg-yellow-200 rounded-lg border-gray-200" type="submit">Logout</button>
                </form>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle ml-4" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Notification
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach (Auth::user()->notifications as $noti)
                            <div class="flex justify-between items-center">
                                <a class="dropdown-item" href="#">{{ $noti->data['message']}}</a>
                                <a class="bg-blue-200 rounded-lg" href="{{ route('makeRead', ['notification' => $noti->id]) }}">Make a read</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endauth
        </div>

        @guest
            <div class=" flex justify-between items-center gap-4">
                <a class="p-3 bg-blue-200 rounded-lg border-gray-200" href="/register">Register</a>
                <a class="p-3 bg-red-200 rounded-lg border-gray-200" href="/login">Login</a>
            </div>
        @endguest

        <button data-collapse-toggle="navbar-language" type="button"
            class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
            aria-controls="navbar-language" aria-expanded="false">
            <span class="sr-only">Open main menu</span>
            <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                viewBox="0 0 17 14">
                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M1 1h15M1 7h15M1 13h15" />
            </svg>
        </button>
    </div>
    <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-language">
    </div>
    </div>
</nav>
