<div class="flex gap-12 justify-center items-center py-4">

    <form class="flex items-center w-full max-w-3xl" action="{{ route('article.search.user') }}" method="get">
        <input type="text" class="bg-gray-100 border-2 border-blue-500 p-2 rounded mr-2 w-full" name="title"
            placeholder="Search for title..." aria-label="Search for title">

        <input type="text" class="bg-gray-100 border-2 border-blue-500 p-2 rounded mr-2 w-full" name="author"
            placeholder="Search for author..." aria-label="Search for author">

        <div class="relative mr-2 w-full">
            <select class="bg-gray-100 border-2 border-blue-500 p-2 rounded w-full" name="categoryName">
                <option value="">Search by category</option>
                @foreach ($categories as $key => $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="relative mr-2 w-full">
            <select class="bg-gray-100 border-2 border-blue-500 p-2 rounded w-full" name="tag">
                <option value="">Search by tag</option>
                @foreach ($tags as $key => $tag)
                    <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                @endforeach
            </select>
        </div>
        <button class="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded" type="submit">
            Search
        </button>
    </form>
</div>
