@extends('dashboard')

@section('content')
    <div class="m-20">


        @if ($errors->any())
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mb-4">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-4">
                {{ session('success') }}
            </div>
        @endif

        <form class="mx-auto mt-12" action="{{ route('writerArticle.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-4">
                <label for="title" class="block text-gray-700 font-bold mb-2">Title:</label>
                <input type="text" class="form-control w-full px-3 py-2 border border-gray-300 rounded-md" name="title"
                    id="title">
            </div>

            <!-- Textarea without sizing class -->
            <div class="mb-4">
                <label class="block text-gray-700 font-bold mb-2" for="shortDescription">Short Description:</label>
                <textarea class="form-control w-full px-3 py-2 border border-gray-300 rounded-md" id="shortDescription"
                    name="shortDescription" rows="3"></textarea>
            </div>

            <div class="mb-4">
                <label for="category_id" class="block text-gray-700 font-bold mb-2">Select a Category:</label>
                <select class="form-control w-full px-3 py-2 border border-gray-300 rounded-md" id="category_id"
                    name="category_id">
                    <option>Choose an option:</option>
                    @foreach ($categories as $key => $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-4">
                <label for="editor" class="block text-gray-700 font-bold mb-2">Write Content:</label>
                <textarea id="editor" name="content"></textarea>
            </div>

            <div class="mb-4">
                <label for="tags" class="block text-gray-700 font-bold mb-2">Select Tags:</label>
                <select multiple class="form-control w-full px-3 py-2 border border-gray-300 rounded-md" id="tags"
                    name="tags[]">
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-6">
                <div class="mb-3">
                    <label for="formFile" class="block text-gray-700 font-bold mb-2">Select an image:</label>
                    <input class="form-control w-full px-3 py-2 border border-gray-300 rounded-md" type="file"
                        id="formFile" name="files" onchange="handleFileSelectStory(event)">
                </div>
                <div id="selectedFiles1"></div>
                <div id="showText"></div>
            </div>

            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                Submit
            </button>
        </form>

        <script src="https://cdn.ckeditor.com/ckeditor5/41.1.0/classic/ckeditor.js"></script>
        <script>
            function handleFileSelectStory(e) {
                var selDiv = document.querySelector("#selectedFiles1");
                var showText = document.querySelector("#showText");
                if (!e.target.files || !window.FileReader) return;
                selDiv.innerHTML = "";
                var files = e.target.files;
                var filesArr = Array.prototype.slice.call(files);
                var yourTextImage = '<p class="mb-2">Images that you selected:</p>';
                showText.innerHTML = yourTextImage;
                filesArr.forEach(function(f, i) {
                    var f = files[i];
                    if (!f.type.match("image.*")) {
                        return;
                    }
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        var html = '<img class="max-w-xs" src="' + e.target.result + '"><br clear="left"/>';
                        selDiv.innerHTML += html;
                    }
                    reader.readAsDataURL(f);
                });
            }

            ClassicEditor
                .create(document.querySelector('#editor'), {
                    ckfinder: {
                        uploadUrl: `{{ route('content.image.upload1') . '?_token=' . csrf_token() }}`
                    }
                })
                .then(editor => {
                    console.log(editor);
                })
                .catch(error => {
                    console.error(error);
                });
        </script>
    </div>
@endsection
