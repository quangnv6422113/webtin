@extends('dashboard')

@section('content')
    <a href="{{ route('writerArticle.create') }}"
        class="ml-20 text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">Create
        an article</a>
    <div class="mt-12 flex justify-between items-center mx-36 gap-6">
        <div class="w-[70%] p-4">
            @foreach ($articles as $article)
                <div
                    class="relative block mb-4 bg-white hover:bg-gray-100 transition duration-200 shadow-lg rounded-lg overflow-hidden group">
                    <div class="flex gap-4">
                        <div class="w-[30%]">
                            <img class="w-full h-auto object-cover" src="{{ Storage::url($article->image) }}"
                                alt="Article Image">
                        </div>
                        <div class="w-[70%] p-4">
                            <h1 class="font-bold text-xl mb-2">{{ $article->title }}</h1>
                            <p class="text-gray-700">{{ $article->shortDescription }}</p>
                        </div>
                    </div>
                    <div
                        class="absolute bottom-4 right-4 flex gap-2 opacity-0 group-hover:opacity-100 transition-opacity duration-200">
                        <a href="{{ route('show.article', ['id' => $article->id]) }}"
                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                            Detail
                        </a>
                        <a href="{{ route('writerArticle.edit', ['writerArticle' => $article->id]) }}"
                            class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded">
                            Edit
                        </a>
                        <form action="{{ route('writerArticle.destroy', ['writerArticle' => $article->id]) }}"
                            method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit"
                                class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                                Delete
                            </button>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
