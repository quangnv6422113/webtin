<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Role;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user1 = User::create([
            'name' => 'quang',
            'email' => 'User@gmail.com',
            'password' => bcrypt('1234'),
        ]);

        $user2 = User::create([
            'name' => 'quang',
            'email' => 'Writer@gmail.com',
            'password' => bcrypt('1234'),
            'role' => Role::WRITER,
        ]);

        $user3 = User::create([
            'name' => 'quang',
            'email' => 'Admin@gmail.com',
            'password' => bcrypt('1234'),
            'role' => Role::ADMIN,
        ]);
    }
}
