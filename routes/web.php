<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\User\ArticleUserController;
use App\Http\Controllers\User\CategoryUserController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Writer\TagController;
use App\Http\Controllers\Writer\ArticleWriterController;
use App\Http\Controllers\User\LikeController;
use App\Http\Controllers\User\CommentController;
use App\Http\Controllers\languageController;
use App\Http\Controllers\SocializationController;

//Route user dont need to login
Route::get('/', [ArticleUserController::class, 'index'])->name('homepage');
Route::get('/category/detail/{id}', [CategoryUserController::class, 'detail'])->name('detail.category');
Route::get('/article/detail/{id}', [ArticleUserController::class, 'show'])->name('show.article');
Route::get('/article/search', [ArticleUserController::class, 'search'])->name('article.search.user');
//route to stranslate
Route::get('/translate/{language}', [LanguageController::class, 'translate'])->name('translate.language');
//route to login with google
Route::get('/redirect/google', [SocializationController::class, 'redirectGoogle'])->name('redirect.google');
Route::get('/auth/google/callback', [SocializationController::class, 'callback']);


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    // make a read
    Route::get('/makeRead/{notification}', [NotificationController::class, 'makeRead'])->name('makeRead');
    //route like an article
    Route::post('/articles/{articleId}/like', [LikeController::class, 'likePost'])->name('like');
    Route::delete('/articles/{articleId}/like', [LikeController::class, 'unlikePost'])->name('unlike');
    Route::resources([
        'comment' => CommentController::class,
    ]);
});

//Route of writer role
Route::prefix('writer')->middleware(['auth', 'checkRole:Writer'])->group(function () {
    Route::post('upload-textImage1', [ArticleWriterController::class, 'uploadTextImage'])->name('content.image.upload1');
    Route::resources([
        'writerArticle' => ArticleWriterController::class,
    ]);
});


//Route of admin role
Route::prefix('admin')->middleware(['auth', 'checkRole:Admin'])->group(function () {
    //Article controller route
    Route::get('edit-post', [ArticleController::class, 'edit2'])->name('edit2');
    Route::get('/create/article', [ArticleController::class, 'crate'])->name('article.create.differ');
    Route::get('/list-schedule', [ArticleController::class, 'listSchedule'])->name('article.list.schedule');
    Route::get('/create-schedule', [ArticleController::class, 'createSchedule'])->name('article.create.schedule');
    Route::post('/store-schedule', [ArticleController::class, 'storeSchedule'])->name('article.store.schedule');
    Route::get('/search-article', [ArticleController::class, 'search'])->name('article.search');
    Route::post('upload-textImage', [ArticleController::class, 'uploadTextImage'])->name('content.image.upload');

    //Routes to search by model filler
    Route::get('/search-user', [UserController::class, 'search'])->name('user.search');
    Route::get('/search-tag', [TagController::class, 'search'])->name('tag.search');
    Route::get('/search-category', [CategoryController::class, 'search'])->name('category.search');
    Route::get('/searc-notification', [NotificationController::class, 'search'])->name('notification.search');

    //all route resource
    Route::resources([
        'categories' => CategoryController::class,
        'users' => UserController::class,
        'article' => ArticleController::class,
        'notification' => NotificationController::class,
        'tag' => TagController::class,
    ]);
});

require __DIR__ . '/auth.php';
