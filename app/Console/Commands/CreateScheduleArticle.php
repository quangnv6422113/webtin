<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Article;

class CreateScheduleArticle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-schedule-article';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $articles = Article::where('publish_at', '<=', now())->where('status', 'dislable')->update(['status' => 'publish']);

        if ($articles) {
            $this->info("Scheduled articles have been published.");
        } else {
            $this->info("No articles to publish.");
        }
    }
}
