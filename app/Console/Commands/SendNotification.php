<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\NotificationUser;
use App\Models\User;
use App\Notifications\SendNotificaton;
use Illuminate\Support\Facades\Notification;
use PDO;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */


    public function handle()
    {
        $notifications = NotificationUser::where('send_at', '<=', now())
            ->where('status', 'dislable')
            ->get();

        if ($notifications->isNotEmpty()) {
            foreach ($notifications as $notification) {
                $usersQuery = User::query();
                switch ($notification->send_to) {
                    case 'User':
                        $usersQuery->where('role', 'User');
                        break;
                    case 'Writer':
                        $usersQuery->where('role', 'Writer');
                        break;
                    case 'All':
                        break;
                    default:
                        $recipient_ids = json_decode($notification->recipient_ids, true);
                        if (is_array($recipient_ids)) {
                            $usersQuery->whereIn('id', $recipient_ids);
                        }
                        break;
                }

                $users = $usersQuery->get();

                if ($users->isNotEmpty()) {
                    Notification::send($users, new SendNotificaton($notification->message));
                    $notification->update(['status' => 'public']);
                }
            }
        }
    }
}
