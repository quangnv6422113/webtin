<?php

namespace App\Observers;

use App\Models\NotificationUser;
use Illuminate\Foundation\Auth\User;
use App\Notifications\SendNotificaton;
use Illuminate\Support\Facades\Notification;

class NotificationObserver
{
    /**
     * Handle the NotificationUser "created" event.
     */
    public function created(NotificationUser $notificationUser): void
    {
        if ($notificationUser->send_to == 'Now') {
            $users = User::all();
            $message = $notificationUser->message;
            Notification::send($users, new SendNotificaton($message, $users));
            $notificationUser->updat(['status' => 'public']);
        }
    }

    /**
     * Handle the NotificationUser "updated" event.
     */
    public function updated(NotificationUser $notificationUser): void
    {
        //
    }

    /**
     * Handle the NotificationUser "deleted" event.
     */
    public function deleted(NotificationUser $notificationUser): void
    {
        //
    }

    /**
     * Handle the NotificationUser "restored" event.
     */
    public function restored(NotificationUser $notificationUser): void
    {
        //
    }

    /**
     * Handle the NotificationUser "force deleted" event.
     */
    public function forceDeleted(NotificationUser $notificationUser): void
    {
        //
    }
}
