<?php

namespace App;

enum Role
{
    const USER = 'User';
    const WRITER = 'Writer';
    const ADMIN = 'Admin';
}
