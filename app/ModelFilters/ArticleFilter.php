<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class ArticleFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */

    public function categoryName($id)
    {
        return $this->where('id', $id);
    }

    public function title($id)
    {
        return $this->where('title', $id);
    }

    public function author($id)
    {
        return $this->where('author', $id);
    }

    public function tag($title)
    {
        return $this->whereHas('tags', function ($query) use ($title) {
            $query->where('name', $title);
        });
    }
}
