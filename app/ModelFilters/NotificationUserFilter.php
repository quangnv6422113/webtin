<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class NotificationUserFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    public function title($id)
    {
        return $this->where('title', 'LIKE', "%$id%");
    }

    public function status($id)
    {
        return $this->where('status', 'LIKE', "%$id%");
    }
    
}
