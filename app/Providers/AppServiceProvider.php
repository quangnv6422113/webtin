<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Category;
use App\Models\Tag;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {

    }

    /**
     * Bootstrap any application services.
     */
    
    public function boot(): void
    {
        View::composer('user*', function ($view) {
            $categories = Category::all();
            $view->with('categories', $categories);
        });
        View::composer('*', function ($view) {
            $tags = Tag::all();
            $view->with('tags', $tags);
        });
    }
}
