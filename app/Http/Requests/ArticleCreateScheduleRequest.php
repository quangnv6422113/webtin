<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleCreateScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'shortDescription' => 'required|string|max:500',
            'content' => 'required|string',
            'files' => 'required|file|mimes:png,jpg,jpeg|max:5120', 
            'category_id' => 'required|exists:categories,id',
            'publish_at' => ['required',
            'date',
            'after:now', ]
        ];
    }
}
