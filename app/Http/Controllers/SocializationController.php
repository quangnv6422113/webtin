<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class SocializationController extends Controller
{

    public function redirectGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callBack()
    {
        try {
            $googleUser = Socialite::driver('google')->user();
            $existingUser = User::where('provider_id', $googleUser->id)->first();
            if ($existingUser) {
                // Đăng nhập người dùng đã tồn tại
                Auth::login($existingUser);
            } else {
                // Kiểm tra nếu email đã tồn tại
                $userByEmail = User::where('email', $googleUser->email)->first();

                if ($userByEmail) {
                    $userByEmail->update([
                        'provider_id' => $googleUser->id,
                        'provider' => 'google',
                    ]);

                    Auth::login($userByEmail);
                } else {
                    // Tạo người dùng mới
                    $newUser = User::create([
                        'name' => $googleUser->name,
                        'email' => $googleUser->email,
                        'provider' => 'google',
                        'provider_id' => $googleUser->id,
                        'password' => Hash::make(uniqid()),
                    ]);

                    Auth::login($newUser);
                }
            }

            return redirect('/');
        } catch (\Exception $e) {
            return redirect('/login')->with('error', 'Đăng nhập Google thất bại, vui lòng thử lại.');
        }
    }
}
