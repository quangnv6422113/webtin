<?php

namespace App\Http\Controllers\Writer;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleWriterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $articles = Article::with('tags')->where('user_id', Auth::user()->id)->get();
        $categories = Category::all();
        $tags = Tag::all();
        return view('writer.article.index', compact('articles', 'categories', 'tags'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('writer.article.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ArticleRequest $request)
    {
        $path = '';
        if ($request->hasFile('files')) {
            $image = $request->file('files');
            $path = Storage::disk('public')->putFile('articles', $image);
        }

        $article = Article::create([
            'title' => $request->input('title'),
            'shortDescription' => $request->input('shortDescription'),
            'content' => $request->input('content'),
            'author' => 'Your Author Name',
            'image' => $path,
            'category_id' => $request->input('category_id'),
            'user_id' => $request->user()->id,
        ]);

        $articleId = $request->tags;
        $article->tags()->attach($articleId);

        return redirect()->back()->with('success', 'Article created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function uploadTextImage(Request $request)
    {
        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;
            $request->file('upload')->move(public_path('media'), $fileName);
            $url = asset('media/' . $fileName);
            return response()->json(['fileName' => $fileName, 'uploaded' => 1, 'url' => $url]);
        }
    }

    public function show(string $id)
    {
        $article = Article::with('tags')->findOrFail($id);
        $articleSameCategories = Article::where('category_id', $article->category_id)
            ->where('id', '!=', $article->id)
            ->get();

        $articleSameTags = Article::whereHas('tags', function ($query) use ($article) {
            $query->whereIn('tags.id', $article->tags->pluck('id'));
        })
            ->where('articles.id', '!=', $article->id)
            ->get();

        return view('writer.article.detail', compact('article', 'articleSameCategories', 'articleSameTags'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $categories = Category::all();
        $tags = Tag::all();
        $article = Article::with('tags')->findOrFail($id);
        return view('admin.article.editArticle', compact('article', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $path = '';
        $article = Article::findOrFail($id);
        if ($request->hasFile('files')) {
            $image = $request->file('files');
            $path = Storage::disk('public')->putFile('articles', $image);
            if (Storage::exists($article->image)) {
                Storage::delete($article->image);
            }
        }
        $article->update([
            'title' => $request->input('title'),
            'shortDescription' => $request->input('shortDescription'),
            'content' => $request->input('content'),
            'author' => 'Your Author Name',
            'image' => $path != '' ? $path : $article->image,
            'category_id' => $request->input('category_id'),
        ]);

        $articleId = $request->tags;
        if ($articleId) {
            $article->tags()->sync($articleId);
        }

        return redirect()->back()->with('success', 'Article updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $article = Article::findOrFail($id);
        if ($article) {
            $article->tags()->delete();
            $article->delete();
        }
        return back()->with('success', 'Delete an article sucessfully');
    }
}
