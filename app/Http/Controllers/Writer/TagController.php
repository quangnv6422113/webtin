<?php

namespace App\Http\Controllers\Writer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Article;
use App\Http\Requests\TagRequest;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tags = Tag::all();
        $articles = Article::all();
        return view('admin.tag.index', compact(['tags', 'articles']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TagRequest $request)
    {
        $tags = Tag::create([
            'name' => $request->input(['name']),
            'description' => $request->input(['description']),
        ]);
        $articleIds = $request->articles;
        if ($articleIds) {
            $tags->articles()->attach($articleIds);
        }
        return back()->with('success', 'create a tag successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    public function search(Request $request)
    {
        $articles = Article::all();
        $tags = Tag::filter($request->all())->get();

        return view('admin.tag.index', compact(['tags', 'articles']));
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(TagRequest $request, string $id)
    {
        $tags = Tag::findOrFail($id);
        $tags->update([
            'name' => $request->input(['name']),
            'description' => $request->input(['description']),
        ]);
        $articleIds = $request->articles;
        if ($articleIds) {
            $tags->articles()->sync($articleIds);
        }
        return back()->with('success', 'Update a tag successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $tags = Tag::findOrFail($id);
        if ($tags) {
            $tags->delete();
            $tags->articles()->detach();
        }
        return back()->with('success', 'Delete a tag successfully');
    }
}
