<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class languageController extends Controller
{
    public function translate(Request $request, $language)

    {
        $locale = in_array($language, ['en', 'vi']) ? $language : 'en';
        Session::put('locale', $locale);
        $locale = App::currentLocale();
        return redirect()->back();
    }
}
