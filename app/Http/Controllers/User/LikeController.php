<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Like;


class LikeController extends Controller
{

    public function likePost($articleId)
    {
        $like = new Like();
        $like->user_id = auth()->id();
        $like->article_id = $articleId;
        $like->save();

        return back();
    }

    public function unlikePost($articleId)
    {
        $article =  Article::findOrFail($articleId);
        $article->likes()->where('user_id', auth()->id())->delete();

        return back();
    }
}
