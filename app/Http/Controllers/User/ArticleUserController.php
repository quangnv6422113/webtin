<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\NotificationUser;
use Illuminate\Support\Facades\Auth;

class ArticleUserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $articles = Article::with('tags')->get();
        
        return view('user.homepage', compact('articles'));
    }

    public function show(string $id)
    {
        $article = Article::with(['comments.user'])->findOrFail($id);
        $articleSameCategories = Article::where('category_id', $article->category_id)
            ->where('id', '!=', $article->id)
            ->get();

        $articleSameTags = Article::whereHas('tags', function ($query) use ($article) {
            $query->whereIn('tags.id', $article->tags->pluck('id'));
        })
            ->where('articles.id', '!=', $article->id)
            ->get();

        return view('user.detail', compact('article', 'articleSameCategories', 'articleSameTags'));
    }

    public function search(Request $request)
    {
        $articles = Article::filter($request->all())->get();

        return view('user.homepage', compact('articles'));
    }
}
