<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Article;

class CategoryUserController extends Controller
{
    public function detail($id)
    {
        $articles = Article::where('category_id', $id)->get();
        
        return view('user.homepage', compact('articles'));
    }
}
