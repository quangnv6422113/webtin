<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = Category::all();

        return view('admin.category.index', compact('categories'));
    }

    public function create()
    {
        //
    }

    public function store(CategoryRequest $request)
    {
        $category = Category::create([
            'name' => $request->input('name'),
            'discription' => $request->input('description'),
        ]);

        return back()->with('Edited a category successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }
    
    public function search(Request $request)
    {
        $categories = Category::filter($request->all())->get();

        return view('admin.category.index', compact('categories'));
    }

    public function update(CategoryRequest $request, string $id)
    {
        $category = Category::findOrFail($id);

        $category->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ]);

        return redirect()->back()->with('success', 'Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = Category::findOrFail($id);
        if ($category) {
            $category->delete();
        }

        return redirect()->back()->with('success', 'Delete updated successfully');
    }

    public function listTrahsed()
    {
        $categories = Category::onlyTrashed()->get();
        return view('admin.category.trashed', compact('categories'));
    }

    public function restore(string $id)
    {
        $category = Category::findOrFail($id);
        if ($category) {
            $category->restore();
        }

        return redirect()->back()->with('success', 'Restored deleted successfully');
    }

    public function deleteForce(string $id)
    {
        $category = Category::findOrFail($id);
        if ($category) {
            $category->forceDelete();;
        }

        return redirect()->back()->with('success', 'Delete deleted successfully');
    }
}
