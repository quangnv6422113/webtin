<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NotificationUser;
use App\Http\Requests\NotificaitonRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     */


    public function sendTo()
    {
        $send = [
            '1' => 'User',
            '2' => 'Writer',
            '3' => 'All',
            '4' => 'Others'
        ];
        return  $send;
    }

    public function index()
    {
        $notifications = NotificationUser::all();
        $sendTos = $this->sendTo();
        $users = User::all();
        return view('admin.notification.index', compact(['notifications', 'sendTos', 'users']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NotificaitonRequest $request)
    {
        $notification = NotificationUser::create([
            'title' => $request->input('title'),
            'message' =>  $request->input('message'),
            'send_to' =>  $request->input('send_to'),
            'status' =>  'dislable',
            'send_at' => $request->input('send_at'),
            'user_id' => $request->user()->id,
            'recipient_ids' => $request->has('recipient_ids') ? json_encode($request->recipient_ids) : null
        ]);
        return back()->with('success', 'Create an notification sucess');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    public function search(Request $request)
    {
        $notifications = NotificationUser::filter($request->all())->get();
        $sendTos = $this->sendTo();

        return view('admin.notification.index', compact(['notifications', 'sendTos']));
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(NotificaitonRequest $request, string $id)
    {
        $notification = NotificationUser::findOrFail($id);

        $notification = NotificationUser::updated([
            'titile' => $request->input('titile'),
            'message' =>  $request->input('message'),
            'send_to' =>  $request->input('send_to'),
            'status' =>  $request->input('status'),
        ]);

        return back()->with('success', 'updated an notification sucess');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $notification = NotificationUser::findOrFail($id);
        if ($notification) {
            $notification->delete();
        }
        return back()->with('success', 'Deleted an notification sucess');
    }

    public function makeRead($id)
    {
        if ($id) {
            $notification = Auth::user()->notifications->where('id', $id)->first();
            $notification->markAsRead();
        }

        return back();
    }
}
