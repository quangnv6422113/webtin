<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use EloquentFilter\Filterable;

class Tag extends Model
{
    use HasFactory, Filterable;
    protected $fillable = [
        'name',
        'description'
    ];
    public function articles(): BelongsToMany
    {
        return $this->belongsToMany(Article::class, 'article_tags');
    }
}
