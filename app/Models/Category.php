<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use EloquentFilter\Filterable;

class Category extends Model
{
    use HasFactory, SoftDeletes, Filterable;
    protected $fillable = [
        'name',
        'discription',
    ];
    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }
}
