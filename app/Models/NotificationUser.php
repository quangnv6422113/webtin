<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use App\Observers\NotificationObserver;
use EloquentFilter\Filterable;

#[ObservedBy([NotificationObserver::class])]
class NotificationUser extends Model
{
    use HasFactory, Filterable;
    protected $fillable = [
        'title',
        'message',
        'send_to',
        'status',
        'send_at',
        'user_id',
        'recipient_ids'
    ];
}
